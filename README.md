I have implemented the assignment in three ways:

    1. Using Django's inbuilt admin panel
    2. Wrote views and templates 
    3. An API using Django REST Framework
    
Installation:

Install Dependencies:

    pip install -r requirements.txt 

Make migrations:

    python manage.py makemigrations


Create tables

    python manage.py migrate 

Create Superuser

    python manage.py createsuperuser 

Test Development Server

    python manage.py runserver    