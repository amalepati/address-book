from django.contrib import admin
from .models import *

admin.site.site_header = "Address book"

class ContactAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'phone_number', 'email', 'street_address')
    list_filter = ('first_name', 'last_name', 'phone_number', 'email', 'street_address')
    search_fields = ('first_name', 'last_name', 'phone_number', 'email', 'street_address')

admin.site.register(Contact, ContactAdmin)