from django.db import models
from django.urls import reverse


# Create your models here.
class Contact(models.Model):

    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=12)
    email = models.EmailField(max_length=70,blank=True,null=True)
    street_address = models.CharField(max_length=30)

    class Meta:
        ordering = ["first_name","last_name"]

    def __str__(self):
        return self.last_name + ',' + self.first_name

    def get_absolute_url(self):
        return reverse('contact_edit', kwargs={'pk': self.pk})