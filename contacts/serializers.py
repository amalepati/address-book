from rest_framework import serializers
from contacts.models import *


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ('id','first_name','last_name','phone_number','email','street_address')
