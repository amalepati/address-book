(function() {
  $(document).on('click', '#inprocessExportCSVBtn, #completeExportCSVBtn, #newExportCSVBtn', function() {
    var elId = $(this).attr('id'),
        exportTableName = 'complete',
        tableId;
    
    if (elId.startsWith('inprocess')) {
      exportTableName = 'inprocess';
    } else if (elId.startsWith('new')) {
      exportTableName = 'new';
    }

    tableId = '#' + exportTableName + 'WO';

    var tableEl = $(tableId),
        _tableData = tableEl.data();

    if (tableEl.data()) {
      var tableData = _tableData['bootstrap.table'].data,
          columns = _tableData['bootstrap.table'].columns,
          headingStringArr = [],
          rowStringArr = [],
          rowsStringArr = [],
          csvData,
          headingsInOrder = ['id', 'property', 'building', 'unit', 'owner', 'creation_date',
                             'priority', 'category', 'equipment', 'description', 'action_taken',
                             'completion_date', 'cost_to_fix', 'concerned_resident', 'resident_contact'];

      for (var column of columns) {
        headingStringArr.push(column.title);
      }

      for (var row of tableData) {
        rowStringArr = [];
        var rowKeys = Object.keys(row);
        rowKeys.sort(function(a, b) {
          return headingsInOrder.indexOf(a) > headingsInOrder.indexOf(b) ? 1 : -1;
        });

        for (var key of rowKeys) {
          if (row[key].length === 0) {
            rowStringArr.push(" ");
          } else {
            rowStringArr.push(row[key]);
          }
        }

        rowsStringArr.push(rowStringArr.join(','));
      }

      csvData = headingStringArr.join(',') + '\n' + rowsStringArr.join('\n');
      exportToCSV(csvData, exportTableName + '_workorders');
    }
  });

  function exportToCSV(csv, fileName) {
    const blob = new Blob(['\uFEFF', csv], {type: 'text/csv'});
    const link = document.createElement('a');
    link.setAttribute('href', window.URL.createObjectURL(blob));
    link.setAttribute('download', fileName + '.csv');
    document.body.appendChild(link); // Required for FF
    link.click();
  }
}());
