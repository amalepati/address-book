from . import views
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter, Route
from rest_framework import routers
from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.conf import settings

# create a router and register our viewsets with it
router = routers.DefaultRouter()
router.register(r'contacts',views.ContactViewSet, 'contacts')

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^$', login_required(login_url=settings.LOGIN_URL)(views.ContactListView.as_view(template_name='index.html')), name='index'),
    url(r'^login/', LoginView.as_view(template_name='login.html'), name='login'),
    url(r'^logout/', login_required(login_url=settings.LOGIN_URL)(LogoutView.as_view()), name='logout'),
    url(r'^new/', views.ContactCreate.as_view(template_name='new_contact.html'), name='contact_new'),
    url(r'^(?P<pk>\d+)/edit/\$', views.ContactUpdate.as_view(template_name='edit_contact.html'), name='contact_edit'),
    url(r'^(?P<pk>\d+)/delete/\$', views.ContactDelete.as_view(template_name='delete_contact.html'), name='contact_delete'),
]