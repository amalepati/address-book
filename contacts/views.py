from rest_framework import viewsets
from contacts.models import *
from contacts.serializers import *
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.urls import reverse_lazy

@method_decorator(login_required(login_url=settings.LOGIN_URL), name='dispatch')
class ContactListView(ListView):
    template_name = 'index.html'
    model = Contact

@method_decorator(login_required(login_url=settings.LOGIN_URL), name='dispatch')
class ContactCreate(CreateView):
    model = Contact
    fields = ['first_name', 'last_name', 'phone_number', 'email', 'street_address']
    success_url = reverse_lazy('index')

@method_decorator(login_required(login_url=settings.LOGIN_URL), name='dispatch')
class ContactUpdate(UpdateView):
    model = Contact
    fields = ['first_name', 'last_name', 'phone_number', 'email', 'street_address']
    success_url = reverse_lazy('index')

@method_decorator(login_required(login_url=settings.LOGIN_URL), name='dispatch')
class ContactDelete(DeleteView):
    model = Contact
    success_url = reverse_lazy('index')

@method_decorator(login_required(login_url=settings.LOGIN_URL), name='dispatch')
class ContactViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

